// Octobre 2024 Arnaud Champollion //
// Graduatux //
// Licence GNU GPL //

// Ce préfixe sera utilisé pour le stockage des paramètres dans le cache

let prefixeAppli = 'graduatux';

// Consignes

const consigneEleveGenerale = "Écris les nombres dans les bulles puis clique sur le bouton valider.";
const consigneEleveErreur = "Il y a une erreur. Corrige-la puis clique sur le bouton valider.";
const consigneEleveErreurs = "Il y a des erreurs. Corrige-les puis clique sur le bouton valider.";
const consigneEleveToutJuste = "Tous les nombres sont justes, bravo.";
const consigneEleveCorrige = "Voici les bonnes réponses.";

// Constituants de la page

const body = document.body;
const divMilieu = document.getElementById('milieu');
const consigne = document.getElementById('texte-consigne');
const divConsigne = document.getElementById('consigne');
const boutonReplierConsigne = document.getElementById('replier-consigne');
const boutonMenu = document.getElementById('bouton-menu');
const boutonStats = document.getElementById('bouton-stats');
const divContenu = document.getElementById('contenu');
const divReussite = document.getElementById('reussite');
const divResultats = document.getElementById('resultats');
const droite = document.getElementById('droite');
const divApropos = document.getElementById('apropos');
const checkbox1 = document.getElementById('checkbox1');
const boutonValider = document.getElementById('bouton-valider');
const boutonSuivant = document.getElementById('bouton-suivant');
const spanTauxReussite = document.getElementById('taux-reussite');
const boutonsMasquables = document.querySelectorAll('.masquable');
const divClavier = document.getElementById('clavier');

const casesTactile = document.querySelectorAll('input[name=choix-clavier]');

// Variables globales

let dragged = null;
let afficherGraduationsNiveau3 = false;
let nombreItems = 0;
let nombreReussites = 0;
let score = 0;
let scorePourcent = 0;
let passage = 'jamais';
let chances = 2;
let modeAppli = 'accueil';
let nombreDeQuestions = 5;
let logUrl=null;
let questionActive=null;
let clavierTactile=false;
let choixMax = false;
let max = undefined;
let aideOn = false;

// Tableaux

let tableauxPas = [0];


// Est-on dans Openboard ? ////
openboard = Boolean(window.widget || window.sankore);
console.log('Openboard ? '+openboard);
if (openboard){
    document.body.classList.add('openboard');
}
///////////////////////////////

// Est-on sur un appareil tactile ?
let tactile = "ontouchstart" in document.documentElement;
if (tactile){
    console.log('Cet appareil est tactile.');
    clavierTactile=true;
    casesTactile.forEach(caseACocher => {
        caseACocher.disabled = true;
    });
}
else {console.log("Cet appareil n'est pas tactile");}

// DEV //
// tactile=true;


///////////////////// Lancement du programme ///////////////////
async function executeFunctions() {
    await verifieStockageLocal();
    await checkUrl();
    await appliqueReglages();
    nouvelExercice();    
}
executeFunctions();
//////////////////////////////////////////////////////////////

// Vérification des paramètres sont dans l'URL
function checkUrl() {
    console.log('Lecture URL');
    let url = window.location.search;
    let urlParams = new URLSearchParams(url);

    let passageUrl = urlParams.get('passage');
    if (passageUrl){
        passage = passageUrl
    }

    let primtux = urlParams.get('primtuxmenu');
    if (primtux){
        body.classList.add('primtux');
    }

    let modeAppliUrl = urlParams.get('mode');
    if (modeAppliUrl){
        modeAppli = modeAppliUrl;
    }

    logUrl = urlParams.get('log');
    if (logUrl){
        tableauxPas = logUrl.split('_').map(Number);
    }

    let nombreDeQuestionsUrl = urlParams.get('nombre-questions');
    if (nombreDeQuestionsUrl){
        nombreDeQuestions = parseInt(nombreDeQuestions);
    }

    const choixMaxUrl = urlParams.get('choix-max');
    if (choixMaxUrl) {
        choixMax = Boolean(choixMaxUrl);
    }

    const maxUrl = urlParams.get('max');
    if (maxUrl) {
        max = parseInt(maxUrl);
    }

}

function appliqueReglages() {

    console.log('---- Application des réglages ----');  

    // Mettre à jour tous les formulaires
    cocheCases();
    mettreAJourChoixPassage();
    mettreAJourInputNombreQuestions();

    // Appliquer les styles
    body.classList.add(modeAppli);
    if (modeAppli!='accueil') {
        body.classList.remove('accueil','accueil-general','accueil-prof','accueil-eleve');
    }

    casesTactile.forEach(caseACocher => {
        caseACocher.checked = clavierTactile;
    });
    
}


function tailleConsigne() {
    divConsigne.classList.toggle('pliee');
    boutonReplierConsigne.classList.toggle('pliee');
}

function entreeTexte(entree,touche) {
    if (touche==='suppr') {
        questionActive.innerHTML = questionActive.innerHTML.slice(0, -1);
    } else if (touche==='ok') {
        visibiliteClavier();
        questionActive = null;
    } else {
        let chaine = questionActive.innerText.replace('\n','');
        if (questionActive && chaine.length < 8) {
            questionActive.innerHTML += entree;
        }
    }
}

function visibiliteMenu(mode){
    if (mode==='ouvre' && menu.style.left==='-415px'){
        menu.style.left='0px';
        menuOn=true;
    }
    else {
        menu.style.left='-415px';
        menuOn=false;
    }
}

function visibiliteStats(mode){
    if (mode==='ouvre' && stats.style.right==='-415px'){stats.style.right='0px';}
    else {stats.style.right='-415px';}
}

function majPassage(valeur,manuel){
    passage = valeur;
    stocke('passage',passage);
    majUrl('passage',passage);
    mettreAJourChoixPassage();

    if(manuel){suivant();}
}


function majPas(manuel,formulaire) {

    console.log("Mise à jour des pas "+"manuel="+manuel+" formulaire = "+formulaire)
    // Récupérer tous les éléments input du formulaire
    let checkboxes;
    
    if (formulaire){
        checkboxes = formulaire.querySelectorAll('input[name="choix-pas"]');
    } else {
        checkboxes = document.querySelectorAll('input[name="choix-pas"]');
    }

    // Réinitialiser le tableau
    tableauxPas = [];
        
        
    // Parcourir chaque checkbox
    checkboxes.forEach(checkbox => {      
      if (checkbox.checked) {
        const valeur = parseInt(checkbox.value);
        tableauxPas.push(valeur);
      }
    });
    
    let pasString = tableauxPas.join('_');
    stocke('pas',pasString);
    majUrl('log',pasString);
    cocheCases();

    if(manuel){suivant();}
  }

function changeClavier(valeur) {
    clavierTactile = valeur;
    casesTactile.forEach(caseACocher => {
        caseACocher.checked = valeur;
    });
    stocke('clavier-tactile',clavierTactile);
}

function mettreAJourInputNombreQuestions() {
    const listeInputs = document.querySelectorAll('.nombre-questions');
    listeInputs.forEach(input => {
        input.value = nombreDeQuestions;
    });
}

function changeChoixMax(valeur) {
    choixMax = valeur;    
    cocheCases();
    stocke('choix-max',choixMax);
    majUrl('choix-max',choixMax);
}

function changeInputMax(valeur) {
    max = parseInt(valeur);
    cocheCases();
    stocke('max',max);
    majUrl('max',max);
}


function cocheCases() {

    let listeDesCases = document.querySelectorAll('input[name="choix-pas"]');

    if (tableauxPas.length === 0) {
        tableauxPas = [0];
    }

    listeDesCases.forEach(caseAcocher => {
        if (tableauxPas.includes(parseInt(caseAcocher.value))){
            caseAcocher.checked = true;
        } else {
            caseAcocher.checked = false;
        }
    });

    listeDesCases = document.querySelectorAll('.choix-max');
    listeDesCases.forEach(caseAcocher => {
        caseAcocher.checked = choixMax;
    });

    listeDesInput = document.querySelectorAll('.input-max');
    listeDesInput.forEach(input => {
        input.value = max || 20;
        input.disabled = !choixMax;
    });
}

function inverseCase(span, event) {
    // Vérifie si le clic vient du <input> (case à cocher) lui-même
    if (event.target.tagName.toLowerCase() === 'input' || event.target.tagName.toLowerCase() === 'label') {
        return;  // Ne fait rien si c'est la case à cocher qui est cliquée directement
    }

    // Sinon, inverse l'état de la case à cocher (si ce n'est pas elle qui a été cliquée)
    let caseAcocher = span.querySelector('input[type="checkbox"]');
    if (caseAcocher) {
        caseAcocher.checked = !caseAcocher.checked;
    }
}


function retourAccueil(){
    body.classList='accueil-general accueil';
    modeAppli = "accueil";
    stocke('mode',modeAppli);
    majUrl('mode',modeAppli);
}


function changeEcranAccueil(mode) {
    body.classList.remove('accueil-general','accueil-prof','accueil-eleve','prof','eleve');
    body.classList.add('accueil-'+mode);
    body.classList.add('accueil');
    modeAppli = mode;
    body.classList.add(mode);
    majUrl('mode',modeAppli);
    stocke('mode',modeAppli);
}

function clicBouton(log) {
    tableauxPas = [log];
    body.classList.remove('accueil','accueil-general','accueil-prof','accueil-eleve');
    body.classList.add(modeAppli);
    cocheCases();
    let pasString = tableauxPas.join('_');
    stocke('pas',pasString);
    majUrl('log',pasString);
    effaceStats();
}

function commencer() {
    body.classList.remove('accueil','accueil-general','accueil-prof','accueil-eleve');
    body.classList.add(modeAppli);
    effaceStats();
}

function effaceStats() { //Remise à zéro
    nombreItems = 0;
    nombreReussites = 0;
    score = 0;
    spanTauxReussite.innerHTML = '';
    divReussite.style.backgroundColor = null
    divResultats.innerHTML = '';
    suivant();
}

function mettreAJourChoixPassage() {
    const radios = document.querySelectorAll('input[name="choix-passage"]');
    radios.forEach(radio => {
        if (radio.value === passage) {
            radio.checked = true; // Coche le bouton radio correspondant à la valeur de passage
        } else {
            radio.checked = false; // Décoche les autres
        }
    });
}

async function verifieStockageLocal() {
    console.log('---- Lecture stockage local ----');

    const stockagePas = await litDepuisStockage('pas');
    if (stockagePas) {
        tableauxPas = stockagePas.split('_').map(Number);
    }

    const stockgePassage = await litDepuisStockage('passage');
    if (stockgePassage){
        passage = stockgePassage;
    }

    let nombreDeQuestionsStockage = await litDepuisStockage('nombre-questions');
    if (nombreDeQuestionsStockage){
        nombreDeQuestions = parseInt(nombreDeQuestionsStockage);
    }

    let modeAppliStockage= await litDepuisStockage('mode');
    if (modeAppliStockage){
        modeAppli = modeAppliStockage;
    }

    let clavierTactileStockage = await litDepuisStockage('clavier-tactile');
    clavierTactile = clavierTactileStockage === 'true';

    const choixMaxStockage = await litDepuisStockage('choix-max');
    if (choixMaxStockage) {
        choixMax = Boolean(choixMaxStockage);
    }

    const maxStockage = await litDepuisStockage('max');
    if (maxStockage) {
        max = parseInt(maxStockage);
    }

}

function valider(valideProf) {

    console.log('VALIDATION MODE PROF = '+valideProf);

    if (questionActive) {
        questionActive.classList.remove('active');
        questionActive=null;
    }


    if (modeAppli != 'prof'){
        chances -= 1;
    }

    const questions = document.querySelectorAll('.question');
    
    let nombreDeReponsesJustes = 0;
    let nombreDeQuestions = questions.length;

    

    questions.forEach(question => {

        let reponse = parseFloat(question.innerHTML.replace(/<[^>]*>/g, '').replace(/[^0-9,.]/g, '').replace(',', '.'));
    
        if (!question.classList.contains('juste')){ // Traitement des questions non répondues ou fausses

            if (reponse === question.parentNode.valeur){
                if (!valideProf){
                    question.classList.add('juste');                    
                }
                question.contentEditable = false;
                nombreDeReponsesJustes +=1;
                if (question.classList.contains('deuxieme-chance')){
                    creeStat(question.parentNode.valeur,'demi');
                } else {
                    creeStat(question.parentNode.valeur,'juste');
                }

            } else {
                question.classList.add('faux');
            }

        } else {
            
            nombreDeReponsesJustes +=1;        
 

        
        } // Si la question a déjà été répondue

    });


    if (chances===0 || nombreDeReponsesJustes===nombreDeQuestions || valideProf){
        boutonSuivant.disabled=false;
        if (!valideProf){
            boutonSuivant.classList.add('warning');
        }
        if (modeAppli != 'prof'){
            boutonValider.disabled=true;
        }

        const reponsesFausses = document.querySelectorAll('.question.faux');

        reponsesFausses.forEach(question => {
            question.classList.remove('faux');
            question.classList.add('corrige');
            question.innerHTML=question.parentNode.valeur.toString().replace('.', ',');
            adapteHauteur(question);
            creeStat(question.parentNode.valeur,'faux');
        });

        if (nombreDeReponsesJustes === nombreDeQuestions) {
            consigne.innerHTML=consigneEleveToutJuste;
        } else {
            consigne.innerHTML=consigneEleveCorrige;
        }

        majScore();

    } else {
        if (nombreDeReponsesJustes === nombreDeQuestions - 1) {
            consigne.innerHTML=consigneEleveErreur;
        } else {
            consigne.innerHTML=consigneEleveErreurs;
        }
    }
         

}

function majNombreQuestions(valeur) {
    nombreDeQuestions = parseInt(valeur);
    mettreAJourInputNombreQuestions();
    stocke('nombre-questions',nombreDeQuestions);
    majUrl('nombre-questions',nombreDeQuestions);
}

function creeStat(valeur,classe) {

    console.log("Résultat "+valeur+" "+classe)

    //Sélection de la ligne courante
    let divCourante = divResultats.lastChild;
    
    // Ajout des nombres à la div courante
    let nouvelleStat = document.createElement('span');
    nouvelleStat.classList.add('stat',classe);
    nouvelleStat.innerHTML = valeur.toString().replace('.', ',');    // Vérification des cases à cocher

        if (tableauxPas.length === 0) {
            tableauxPas = [0];
        }
    nouvelleStat.valeur = valeur;
    divCourante.appendChild(nouvelleStat);
    rangeContenu(divCourante);

    nombreItems +=1;
    nombreReussites += classe === 'juste';
    nombreReussites += (classe === 'demi')/2;


}

function majScore() {

    score = nombreReussites / nombreItems;
    scorePourcent = Math.round(score * 100);
    spanTauxReussite.innerHTML = scorePourcent + '%';

    // Calcul de la couleur
    let r, g, b = 0;  // Pas de composante bleue

    if (scorePourcent < 50) {
        // De 0% à 50% : transition du rouge (255, 0, 0) vers le jaune (255, 255, 0)
        r = 255;
        g = Math.round(255 * (scorePourcent / 50));  // Le vert augmente avec le taux de réussite
    } else {
        // De 50% à 100% : transition du jaune (255, 255, 0) vers le vert (0, 255, 0)
        r = Math.round(255 * (1 - (scorePourcent - 50) / 50));  // Le rouge diminue
        g = 255;
    }
    
    // Appliquer la couleur à divReussite
    divReussite.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;
    
    // Calcul de la luminosité (0.299 * R + 0.587 * G + 0.114 * B)
    let luminosity = 0.299 * r + 0.587 * g + 0.114 * b;

    // Choisir noir ou blanc en fonction de la luminosité
    let textColor = (luminosity > 128) ? 'black' : 'white';

    // Appliquer la couleur du texte à divReussite
    divReussite.style.color = textColor;
}

function rangeContenu(conteneur) {
    // Créer un tableau d'enfants
    let enfants = Array.from(conteneur.children);    
    // Trier les enfants selon leur propriété "valeur"
    enfants.sort((a, b) => {
        return a.valeur - b.valeur; // Tri par ordre croissant
    });
    // Effacer le contenu existant
    conteneur.innerHTML = '';
    // Réinsérer les enfants triés dans le conteneur
    enfants.forEach(enfant => conteneur.appendChild(enfant));
}


function suivant() {
    droite.innerHTML='';
    nouvelExercice();
}



function fin(auto) {
    if (auto || confirm("Arrêter l'exercice en cours ?")){
    boutonsMasquables.forEach(bouton => {
        bouton.classList.add('hide');
    });
    divOptions.classList.add('hide');
    listeMots = [];
    divFin.classList.remove('hide');
    }
}

function nouvelExercice() {

    aideOn = false;

    consigne.innerHTML=consigneEleveGenerale;

    chances = 2;

    if (modeAppli != 'prof'){
        boutonSuivant.disabled=true;
        boutonSuivant.classList.remove('warning');        
    } else {
        boutonSuivant.disabled=false;
    }
    boutonValider.disabled=false;

    let nouvelleDiv = document.createElement('div');
    divResultats.appendChild(nouvelleDiv);


    // Tirage au sort du pas

    const logarithme = tirerDans(tableauxPas);
    console.log("logarithme "+logarithme);
    
    const pas = Math.pow(10, logarithme);
    console.log("pas "+pas);

    const grandPas = Math.pow(10, logarithme+1);
    console.log("grandPas "+grandPas);

    let nbDebut;
    let multipleMin;
    let multipleMax;
    if (passage==='toujours'){
        multipleMin = 9;
        multipleMax = 10;
    } else if (passage==='parfois'){
        multipleMin = 3;
        multipleMax = 10;
    } else {
        multipleMin = 1;
        multipleMax = 8;
    }

    let reste;

    if (choixMax && max!=undefined) {
        console.log('max = '+max);
        console.log('pas ='+pas);
        console.log('Grand pas ='+grandPas);
        let limite = Math.floor(max/grandPas)-1;
        if (limite < 1){limite=1};
        reste = Math.floor((max % grandPas) / pas);
        console.log("limite = "+limite);
        console.log("reste = "+reste);
        multipleMax = Math.min(multipleMax,limite);
        console.log('multipleMax ='+multipleMax);
        multipleMin = 1;
    }

    let multipleDebut = tirerNombreEntier(multipleMin,multipleMax);
    console.log('multipleDebut = '+multipleDebut);

    nbDebut = grandPas * multipleDebut;


    // Création des graduations

    let nombreIntervallesNiveau2 = 21;
    let nombreGraduationsNiveau2 = nombreIntervallesNiveau2 + 1;
    let decalagePremiereGraduationNiveau1;
    
    if (choixMax && max!=undefined && multipleDebut === multipleMax) {
        console.log("calcul decalage minimum");
        decalagePremiereGraduationNiveau1 = tirerNombreEntier(10-reste,10);
    } else {
        console.log("calcul decalage minimum entre 3 et 8");
        decalagePremiereGraduationNiveau1 = tirerNombreEntier(3,8);
    }
    
    
    console.log('decalage premiere graduation '+decalagePremiereGraduationNiveau1);
    let decalagePremiereGraduationNiveau2 = 10 - decalagePremiereGraduationNiveau1;

    let ecart = 100 / (nombreGraduationsNiveau2);

    if (logarithme <  0 && afficherGraduationsNiveau3){
        creeGraduationsNiveau3(0, ecart); // Premières graduations de niveau 3
    }

    for (i = 0; i < nombreIntervallesNiveau2; i++) { // Boucle de création des graduations
        
        let nouvelleGraduation = document.createElement('div');
        nouvelleGraduation.classList.add('graduation');

        let valeur = nbDebut - pas*(decalagePremiereGraduationNiveau1-i);


        if ((i - decalagePremiereGraduationNiveau1) % 10 === 0){
            let nombreDecimales = Math.max(0, Math.floor(1 - logarithme)); // S'assurer que c'est un entier positif
            nouvelleGraduation.valeur = parseFloat(valeur.toFixed(nombreDecimales));
            nouvelleGraduation.classList.add('niveau1');
            nouvelleEtiquette = document.createElement('div');
            nouvelleEtiquette.classList.add('etiquette');
            nouvelleEtiquette.innerHTML= nouvelleGraduation.valeur.toString().replace('.', ',');
            nouvelleGraduation.appendChild(nouvelleEtiquette);
        } else {
            let nombreDecimales = Math.max(0, Math.floor(-logarithme)); // S'assurer que c'est un entier positif
            nouvelleGraduation.valeur = parseFloat(valeur.toFixed(nombreDecimales));
            nouvelleGraduation.classList.add('niveau2');
            if ((i - decalagePremiereGraduationNiveau1) % 5 === 0){
                nouvelleGraduation.classList.add('multiple5');
            }
        }
        let position = ecart * (i + 1) ;
        nouvelleGraduation.style.left = position + "%";
        if (logarithme <  0 && afficherGraduationsNiveau3){
            creeGraduationsNiveau3(position, ecart); // Graduations de niveau 3 intermédiaires
        }
        droite.appendChild(nouvelleGraduation);        
    }



    function creeGraduationsNiveau3(position, ecart) {
        for (let k=0; k<9; k++) {
            let nouvelleGraduationNiveau3 = document.createElement('div');
            nouvelleGraduationNiveau3.classList.add('graduation','niveau3');
            nouvelleGraduationNiveau3.style.left =  (position + ecart*(k+1)/ 10) + "%";
            droite.appendChild(nouvelleGraduationNiveau3);
        }
    }

    genererLesQuestions();

}

function genererLesQuestions() {
    const graduationsNiveau2 = Array.from(document.querySelectorAll('.niveau2'));
    const graduationsChoisies = [];
    const valeursChoisies = [];

    // Tirage aléatoire de graduations
    while (graduationsChoisies.length < nombreDeQuestions) {
        const indexAleatoire = Math.floor(Math.random() * graduationsNiveau2.length);
        const graduationChoisie = graduationsNiveau2[indexAleatoire];
    
        // Vérifiez si la graduation n'est pas déjà choisie
        if (!graduationsChoisies.includes(graduationChoisie)) {
            let estConsecutif = false;
    
            // Vérifiez que la graduation n'est pas adjacente à une des graduations déjà choisies
            for (let i = 0; i < graduationsChoisies.length; i++) {
                const indexGraduationChoisie = graduationsNiveau2.indexOf(graduationChoisie);
                const indexGraduationDejaChoisie = graduationsNiveau2.indexOf(graduationsChoisies[i]);
    
                if (Math.abs(indexGraduationChoisie - indexGraduationDejaChoisie) === 1) {
                    estConsecutif = true;
                    break;
                }
            }
    
            // Si aucune graduation adjacente n'est trouvée, ajoutez la nouvelle graduation
            if (!estConsecutif) {
                graduationsChoisies.push(graduationChoisie);
                valeursChoisies.push(graduationChoisie.valeur);
            }
        }
    }
    
    graduationsChoisies.sort((a, b) => a.valeur - b.valeur);


    let lettres = ['A','B','C','D','E','F','G'];
    let index = 0;

    // Affichage des questions
    graduationsChoisies.forEach(graduation => {
        
        // Crée la nouvelle zone de texte
        let nouvelleZoneTexte = document.createElement('div');
        nouvelleZoneTexte.classList.add('etiquette', 'question');
        if (modeAppli!='prof' && !tactile){
            nouvelleZoneTexte.contentEditable=true;
        }        
        
        
        nouvelleZoneTexte.addEventListener('keypress', gererEntreeTexte);

        nouvelleZoneTexte.addEventListener('focus', function() {
            if (nouvelleZoneTexte.classList.contains('faux')) {
                nouvelleZoneTexte.classList.remove('faux');
                nouvelleZoneTexte.classList.add('deuxieme-chance');
                if (!clavierTactile){
                    nouvelleZoneTexte.innerHTML = '<br>';
                }
            }
        });

        nouvelleZoneTexte.addEventListener('click', function() {
            if (modeAppli==='prof') {
                nouvelleZoneTexte.classList.toggle('corrige');
                if (nouvelleZoneTexte.classList.contains('corrige')){
                    nouvelleZoneTexte.innerHTML=nouvelleZoneTexte.parentNode.valeur.toString().replace('.', ',');
                    adapteHauteur(nouvelleZoneTexte);
                } else {
                    nouvelleZoneTexte.innerHTML = '';
                }
            }
        });


        if (!clavierTactile){
            nouvelleZoneTexte.innerHTML='<br>';
        }
        // Ajoute la nouvelle zone texte à l'élément parent    
        graduation.appendChild(nouvelleZoneTexte);


        let nouveauRepere = document.createElement('div');
        nouveauRepere.classList.add('repere');
        let lettre = lettres[index]; 
        nouveauRepere.innerHTML=lettre;
        graduation.appendChild(nouveauRepere);


        adapteHauteur(nouvelleZoneTexte);

        index +=1;
        
    });

    graduationsNiveau2.forEach(graduation => {

        if (!valeursChoisies.includes(graduation.valeur)) { // On exclut les valeurs des questions

            let nouvelleEtiquette = document.createElement('span');
            nouvelleEtiquette.classList.add('etiquette-valeur-petite','transparent');

            let nouveauTexte = document.createElement('span');
            nouveauTexte.classList.add('texte');
            nouveauTexte.innerHTML = graduation.valeur.toString().replace('.',',');

            nouvelleEtiquette.appendChild(nouveauTexte);
            graduation.appendChild(nouvelleEtiquette);

            adapteHauteurEtiquettePetite(nouvelleEtiquette);

        }
    });
        
    

}

function aide() {
    if (!aideOn) {
        let divCourante = divResultats.lastChild;
        let nouvelleBalise = document.createElement('img');
        nouvelleBalise.src = 'images/help.svg';
        nouvelleBalise.classList.add('help');
        divCourante.appendChild(nouvelleBalise);
    }

    aideOn = true;
    const etiquettesaide = document.querySelectorAll('.etiquette-valeur-petite');
    etiquettesaide.forEach(etiquette => {
        etiquette.classList.toggle('transparent');
    });
    const connecteurs = document.querySelectorAll('.connecteur-bas');
    connecteurs.forEach(connecteur => {
        connecteur.classList.toggle('transparent');
    });
}

function adapteHauteurEtiquettePetite(etiquette) {
    let texte = etiquette.firstChild;
    const marge = 5; // Marge de séparation entre les étiquettes
    let tousLesTextes = document.querySelectorAll('.texte, .niveau1 .etiquette');
    let collisionTrouvee = true;
    let deplacement = 0;
    let tours = 0;
    let currentTop = parseFloat(window.getComputedStyle(etiquette).top) || 0;

    while (collisionTrouvee && tours < 5) {
        collisionTrouvee = false;
        tousLesTextes.forEach(autreTexte => {
            if (autreTexte !== texte) {
                console.log(texte.innerHTML, ' vs ', autreTexte.innerHTML);  // Affichage des textes en conflit

                let rect1 = texte.getBoundingClientRect();
                let rect2 = autreTexte.getBoundingClientRect();


                if (!(
                    rect1.bottom + marge < rect2.top ||   // rect1 est au-dessus de rect2 (pas de collision verticalement)
                    rect1.top - marge > rect2.bottom ||   // rect1 est en dessous de rect2 (pas de collision verticalement)
                    rect1.right + marge < rect2.left ||   // rect1 est à gauche de rect2 (pas de collision horizontalement)
                    rect1.left - marge > rect2.right      // rect1 est à droite de rect2 (pas de collision horizontalement)
                )) {
                    // Si aucune de ces conditions n'est vraie, alors il y a une collision
                    console.log(rect1);  // Affichage des positions rect1
                    console.log(rect2);  // Affichage des positions rect2
                    console.log("top actuel",currentTop);
                    currentTop += 10 + marge;
                    etiquette.style.top = currentTop+ 'px'; // Décale de 10px + marge
                    console.log("nouveau top",currentTop);
                    deplacement += 10 + marge;
                    collisionTrouvee = true; // Continue la boucle tant qu'il y a une collision
                }
                
            }
        });
        tours += 1;
    }

    //Ajuste la hauteur du connecteur en retirant la marge ajoutée
    let connecteur = document.createElement('div');
    connecteur.classList.add('connecteur-bas','transparent');
    connecteur.style.height = (deplacement) + 'px';
    let graduation = etiquette.parentNode;
    graduation.appendChild(connecteur);

    console.log('Déplacement total:', deplacement - marge);  // Affichage du déplacement total
}



function adapteHauteur(question) {
    let graduation = question.parentNode;
    // Vérifie les collisions et ajuste la position si nécessaire
    let toutesLesZonesTexte = document.querySelectorAll('.etiquette.question');
    let collisionTrouvee = true;
    let deplacement = 0; // Variable pour suivre le décalage vertical

    while (collisionTrouvee) {
        collisionTrouvee = false; // Réinitialise à faux avant chaque vérification
        toutesLesZonesTexte.forEach(autreZoneTexte => {
            if (autreZoneTexte !== question) {
                // Vérifie si les éléments se chevauchent
                let rect1 = question.getBoundingClientRect();
                let rect2 = autreZoneTexte.getBoundingClientRect();

                if (!(rect1.bottom < rect2.top || rect1.top > rect2.bottom || rect1.right < rect2.left || rect1.left > rect2.right)) {
                    // Si une collision est détectée, ajuster la position en augmentant 'bottom'
                    let currentBottom = parseFloat(window.getComputedStyle(question).bottom) || 0;
                    question.style.bottom = (currentBottom + 10) + 'px'; // Décale de 10px vers le haut
                    deplacement += 10; // Incrémente le décalage vertical
                    collisionTrouvee = true; // Continue la boucle tant qu'il y a une collision
                }
            }
        });
    }

    // Crée le div de connexion
    let connecteur = document.createElement('div');
    connecteur.classList.add('connecteur'); // Ajoute la classe CSS pour le connecteur
    connecteur.style.height = deplacement + 'px'; // Hauteur correspond au décalage
    //connecteur.style.bottom = (parseFloat(window.getComputedStyle(nouvelleZoneTexte).bottom) - deplacement) + 'px'; // Position en bas de la nouvelle zone texte

    // Ajoute le connecteur à l'élément parent
    graduation.appendChild(connecteur);
}

function gererEntreeTexte(event) {
    // tableau des valeurs autorisées : uniquement les chiffres, points et virgules
    const valeursAutorises = Array.from("1234567890.,");

    let chaine = event.target.innerText.replace('\n','');
    let nombreDeCaracteres = chaine.length
    console.log(nombreDeCaracteres + " caractères");
    console.log(chaine);
    // Vérification des caractères non autorisés et du nombre de caractères
    if (!valeursAutorises.includes(event.key) || nombreDeCaracteres > 7) {
        event.preventDefault();
        return;
    }

    adapteHauteur(event.target);
}

function visibiliteClavier(visible) {
    if (visible) {

        

        divClavier.classList.remove('hide');
        let positiony = questionActive.parentNode.offsetTop + droite.offsetTop - questionActive.offsetHeight - 15;
        let positionx = questionActive.parentNode.offsetLeft + droite.offsetLeft;

        // Calculer la position initiale
        divClavier.style.top = (positiony - divClavier.offsetHeight) + "px"; // Juste au-dessus de la question
        divClavier.style.left = positionx + "px";   
        divClavier.style.position = 'absolute';

        // Obtenir la largeur du clavier et de la fenêtre
        let clavierWidth = divClavier.offsetWidth;
        let windowWidth = window.innerWidth;

        // Vérifier si le clavier sort à droite de la fenêtre
        if (positionx + clavierWidth > windowWidth) {
            divClavier.style.left = (windowWidth - clavierWidth/2 - 10) + "px"; // Le déplacer pour qu'il ne dépasse pas à droite
        }

        // Vérifier si le clavier sort à gauche de la fenêtre
        if (positionx < clavierWidth/2) {
            divClavier.style.left = (clavierWidth/2 + 10) + "px"; // Le déplacer pour qu'il ne dépasse pas à gauche
        }

    } else {
        divClavier.classList.add('hide');
    }
}



function clic(event){

    console.log('clic');
    let cible = event.target;

    if (cible!=menu && !estEnfantDe(cible,menu) && cible !=boutonMenu){visibiliteMenu('ferme');}
    if (cible!=menu && !estEnfantDe(cible,stats) && cible !=boutonStats){visibiliteStats('ferme');}


    
    if (cible.classList.contains('draggable')) {
        posX = event?.targetTouches?.[0]?.clientX || event.clientX;
        posY = event?.targetTouches?.[0]?.clientY || event.clientY;
        dragged = cible;
        event.preventDefault();
        posX_objet = dragged.offsetLeft;
        posY_objet = dragged.offsetTop;
        diffsourisx = (posX - posX_objet);
        diffsourisy = (posY - posY_objet);
        dragged.classList.add('dragged');
    }

    if (clavierTactile && modeAppli === 'eleve') {
        if (cible.classList.contains('question') && !cible.classList.contains('juste') && !cible.classList.contains('corrige') && cible != questionActive) {
            if (questionActive) {
                questionActive.classList.remove('active');
            }
            questionActive=cible;
            questionActive.classList.add('active');
            visibiliteClavier(true);
            if (cible.classList.contains('faux')) {
                cible.classList.remove('faux');
                cible.innerHTML='';
            }
        } else if (cible!=divClavier && !estEnfantDe(cible,divClavier)){
            console.log('clic en dehors du clavier')
            if (questionActive) {
                questionActive.classList.remove('active');
            }
            questionActive=null;
            visibiliteClavier(false);
        }
    }

}

function move(event) {
    event.preventDefault();  // Empêche le comportement par défaut (utile pour les événements tactiles)

    // Vérifie si un élément est en train d'être "dragged"
    if (dragged) {
        console.log('dragged');
        
        // Obtention des coordonnées en mode tactile ou souris
        const posX = event?.targetTouches?.[0]?.clientX || event.clientX;
        const posY = event?.targetTouches?.[0]?.clientY || event.clientY;

        console.log('posX ' + posX);

        // Calcul des nouvelles positions de l'élément en fonction de la différence entre la souris et l'élément
        dragged.style.left = (posX - diffsourisx) + "px";
        dragged.style.top = (posY - diffsourisy) + "px";
    }
}


function release(event) {
    if (dragged) {
        dragged.classList.remove('dragged');
        dragged=null;
    }

}

// Tactile
document.addEventListener("touchstart", clic);
document.addEventListener("touchend", release);
document.addEventListener('touchmove', function(event) {
    event.preventDefault();
    move(event);
}, { passive: false });
// Souris
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);


function checkWindowSize() {
    // Ajoute ou enlève la classe "pliee" en fonction de la hauteur de la fenêtre
    if (window.innerHeight <= 465) {
        divConsigne.classList.add('pliee');
        boutonReplierConsigne.classList.add('pliee');
    } else {
        divConsigne.classList.remove('pliee');
        boutonReplierConsigne.classList.remove('pliee');
    }
}

document.addEventListener('DOMContentLoaded', function() {
    // Vérifie la taille de la fenêtre au chargement de la page
    checkWindowSize();
    
    // Vérifie la taille de la fenêtre à chaque redimensionnement
    window.addEventListener('resize', checkWindowSize);
});

